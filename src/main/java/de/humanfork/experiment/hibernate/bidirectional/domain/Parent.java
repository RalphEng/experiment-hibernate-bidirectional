package de.humanfork.experiment.hibernate.bidirectional.domain;

import java.util.List;

public interface Parent <C extends Child<?>> extends PersonEntity {

    List<C> getChildren();
    
    void setChildren(List<C> children);
}
