/**
 * Entity Domain Class for a Hibernate setup with {@code mappedBy} at  {@code Parent.children} field.
 * 
 * <pre><code>
 * {@literal @}Entity
 * public class Parent {
 * 
 *    //mappedBy!
 *    {@literal @}OneToMany(mappedBy = "parent")
 *    private List<ChildMbP> children;
 * }
 * </code></pre>
 * 
 * <pre><code>
 * {@literal @}Entity
 * public class Child {
 * 
 *    {@literal @}ManyToOne
 *    private ParentMbP parent;
 * }
 * </code></pre>  
 */
package de.humanfork.experiment.hibernate.bidirectional.domain.mappedby;
