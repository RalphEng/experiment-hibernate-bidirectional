/**
 * Entity Domain Class for a Hibernate setup <b>without</b> {@code mappedBy} at {@code Parent.children} field
 * but with JoinColumn.
 * 
 * <pre><code>
 * {@literal @}Entity
 * public class Parent {
 * 
 *    //without mappedBy!
 *    {@literal @OneToMany
 *    {@literal @JoinColumn(name = "parentFK", referencedColumnName = "parentID")
 *    private List<ChildMbP> children;
 * }
 * </code></pre>
 * 
 * <pre><code>
 * {@literal @}Entity
 * public class Child {
 *   
 *    //Pay attention to insertable = false, updatable = false 
 *    {@literal @}ManyToOne
 *    {@literal @}JoinColumn(name = "parentFK", insertable = false, updatable = false) 
 *    private Parent parent;
 * }
 * </code></pre>  
 */
package de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn;
