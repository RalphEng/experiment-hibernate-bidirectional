package de.humanfork.experiment.hibernate.bidirectional.domain;

public interface Child <P extends Parent<?>> extends PersonEntity {

    P getParent();
    
    void setParent(P parent);
}
