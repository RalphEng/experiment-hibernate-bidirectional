package de.humanfork.experiment.hibernate.bidirectional.domain;


/**
 * A Person Entity has a Id and a Name.
 */
public interface PersonEntity {

    Integer getId();
    
    String getName();
}
