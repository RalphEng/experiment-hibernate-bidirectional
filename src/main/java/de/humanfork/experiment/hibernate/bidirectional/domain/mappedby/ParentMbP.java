package de.humanfork.experiment.hibernate.bidirectional.domain.mappedby;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import de.humanfork.experiment.hibernate.bidirectional.domain.Parent;
import de.humanfork.experiment.hibernate.bidirectional.domain.PersonEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Parent Class for Setup: "Mapped-By Parent".
 */
@Entity
@ToString(of = "name")
public class ParentMbP implements PersonEntity, Parent<ChildMbP> {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Getter
    private Integer id;
    
    @Getter
    private String name;
    
    @OneToMany(mappedBy = "parent")
    @Getter
    @Setter
    private List<ChildMbP> children;

    /**
     * @deprecated only Hibernate.
     */
    @Deprecated
    protected ParentMbP() {
        super();
    }

    public ParentMbP(String name) {
        super();
        this.name = name;
    }

}
