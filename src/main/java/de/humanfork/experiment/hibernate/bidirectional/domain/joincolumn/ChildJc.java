package de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.humanfork.experiment.hibernate.bidirectional.domain.Child;
import de.humanfork.experiment.hibernate.bidirectional.domain.PersonEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString(of = "name")
public class ChildJc implements PersonEntity, Child<ParentJc> {
    
    @Id    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Getter
    private Integer id;
    
    @Getter
    private String name;

    @ManyToOne
    @JoinColumn(name = "parentFK", insertable = false, updatable = false) 
    @Getter
    @Setter
    private ParentJc parent;
    
    /**
     * @deprecated only Hibernate.
     */
    @Deprecated
    ChildJc() {
        super();
    }

    public ChildJc(String name) {
        this.name = name;
    }
    
}
