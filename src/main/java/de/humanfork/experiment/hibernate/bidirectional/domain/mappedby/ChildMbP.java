package de.humanfork.experiment.hibernate.bidirectional.domain.mappedby;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import de.humanfork.experiment.hibernate.bidirectional.domain.Child;
import de.humanfork.experiment.hibernate.bidirectional.domain.PersonEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString(of = "name")
public class ChildMbP implements PersonEntity, Child<ParentMbP> {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Getter
    private Integer id;
    
    @Getter
    private String name;

    @ManyToOne
    @Getter
    @Setter
    private ParentMbP parent;
    
    /**
     * @deprecated only Hibernate.
     */
    @Deprecated
    ChildMbP() {
        super();
    }

    public ChildMbP(String name) {
        this.name = name;
    }
    
}
