package de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import de.humanfork.experiment.hibernate.bidirectional.domain.Parent;
import de.humanfork.experiment.hibernate.bidirectional.domain.PersonEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Parent Class for Setup: "Mapped-By Parent".
 */
@Entity
@ToString(of = "name")
public class ParentJc implements PersonEntity, Parent<ChildJc> {
    
    @Id
    @Column(name = "parentID")
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Getter
    private Integer id;
    
    @Getter
    private String name;
    
    @OneToMany
    @JoinColumn(name = "parentFK")
    @Getter
    @Setter
    private List<ChildJc> children;

    /**
     * @deprecated only Hibernate.
     */
    @Deprecated
    protected ParentJc() {
        super();
    }

    public ParentJc(String name) {
        super();
        this.name = name;
    }

}
