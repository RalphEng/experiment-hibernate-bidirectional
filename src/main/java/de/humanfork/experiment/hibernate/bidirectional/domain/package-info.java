/**
 * Entity Domain Class for different Jpa/Hiberante Setups.
 * <p>
 * The general Domain Classes are the same for each Setup:
 * There is a parent and its children: {@code Parent <>--- Child}
 * </p>
 * With a bidirectional relation between them:
 * <ul>
 *   <li>Parent class has the field: {@value List<Child>children}</li>
 *   <li>Clild class has the field: {@value Parent parent</li>
 * </ul>
 * The difference between the setups is the Hibernate mapping of that bidirectional relationship.
 * 
 * <p>
 * <i>To separate the domain classes from the different setups, all domain classes have a postfix.</i> 
 * </p>
 */
package de.humanfork.experiment.hibernate.bidirectional.domain;
