package de.humanfork.experiment.hibernate.bidirectional.domain.testsupport;

import java.util.Objects;

import org.assertj.core.api.AbstractAssert;

import de.humanfork.experiment.hibernate.bidirectional.domain.PersonEntity;

public class PersonEntityAssert extends AbstractAssert<PersonEntityAssert, PersonEntity> {

    protected PersonEntityAssert(PersonEntity actual) {
        super(actual, PersonEntityAssert.class);
    }

    public static PersonEntityAssert assertThat(PersonEntity actual) {
        return new PersonEntityAssert(actual);
    }

    public PersonEntityAssert hasName(String name) {
        isNotNull();

        if (!Objects.equals(actual.getName(), name)) {
            failWithMessage("Expected person's name to be <%s> but was <%s>", name, actual.getName());
        }

        return this;
    }

    public PersonEntityAssert hasId(Integer id) {
        isNotNull();

        if (!Objects.equals(actual.getId(), id)) {
            failWithMessage("Expected person's id to be <%s> but was <%s>", id, actual.getId());
        }

        return this;
    }

    public PersonEntityAssert hasEqualNameAndId(PersonEntity person) {
        return hasName(person.getName()).hasId(person.getId());
    }

}
