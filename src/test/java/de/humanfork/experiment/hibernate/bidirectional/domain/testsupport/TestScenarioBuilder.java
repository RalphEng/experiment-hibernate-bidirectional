package de.humanfork.experiment.hibernate.bidirectional.domain.testsupport;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import de.humanfork.experiment.hibernate.bidirectional.domain.Child;
import de.humanfork.experiment.hibernate.bidirectional.domain.Parent;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TestScenarioBuilder<P extends Parent<C>, C extends Child<P>> {

    @FunctionalInterface
    public static interface PersonFactory<T> {
        T build(String name);
    }

    @NonNull
    private final EntityManager entityManager;

    @NonNull
    private final PersonFactory<P> parentFactory;

    @NonNull
    private final PersonFactory<C> childFactory;

    public TestScenario<P, C> build(final boolean parentChildRelation, final boolean childParentReleation) {
        String parentName = "p1";
        P parent = this.parentFactory.build(parentName);

        String childName = "c1";
        C child = this.childFactory.build(childName);

        this.entityManager.persist(parent);
        this.entityManager.persist(child);

        if (parentChildRelation) {
            parent.setChildren(new ArrayList<>(List.of(child)));
        } else {
            parent.setChildren(new ArrayList<>(List.of()));
        }

        if (childParentReleation) {
            child.setParent(parent);
        }

        /* precondition, check the scenario */
        RelationAssert<P, C> relationAssert = RelationAssert.assertThat(parent, child);
        if (parentChildRelation) {
            relationAssert.hasParentChildReleation();
        } else {
            relationAssert.doesNotHaveParentChildReleation();
        }
        if (childParentReleation) {
            relationAssert.hasChildParentReleation();
        } else {
            relationAssert.doesNotHaveChildParentReleation();
        }

        return new TestScenario<P, C>(parent, child);
    }
}
