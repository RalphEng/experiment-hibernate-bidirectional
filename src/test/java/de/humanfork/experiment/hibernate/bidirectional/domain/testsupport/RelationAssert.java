package de.humanfork.experiment.hibernate.bidirectional.domain.testsupport;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import de.humanfork.experiment.hibernate.bidirectional.domain.Child;
import de.humanfork.experiment.hibernate.bidirectional.domain.Parent;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class RelationAssert<P extends Parent<C>, C extends Child<P>>
        extends AbstractAssert<RelationAssert<P, C>, RelationAssert.RelationMember<P, C>> {

    @RequiredArgsConstructor
    @NonNull
    static class RelationMember<P extends Parent<C>, C extends Child<P>> {
        private final P parent;

        private final C child;
    }

    protected RelationAssert(final P parent, final C child) {
        super(new RelationMember<P, C>(parent, child), RelationAssert.class);
    }

    public static <P extends Parent<C>, C extends Child<P>> RelationAssert<P, C> assertThat(final P parent,
            final C child) {
        return new RelationAssert<>(parent, child);
    }

    public RelationAssert<P, C> hasParentChildReleation() {
        isNotNull();
        Assertions.assertThat(this.actual.parent.getChildren())
                .as("Expected parent.children to contain the child")
                .isNotNull().contains(this.actual.child);
        return this;
    }

    public RelationAssert<P, C> doesNotHaveParentChildReleation() {
        isNotNull();
        Assertions.assertThat(this.actual.parent.getChildren())
                .as("Expected parent.children to not contain the child")
                .isNotNull().doesNotContain(this.actual.child);
        return this;
    }

    public RelationAssert<P, C> hasChildParentReleation() {
        isNotNull();
        Assertions.assertThat(this.actual.child.getParent())
                .as("Expected child to have a relation to its parent")
                .isEqualTo(this.actual.parent);
        return this;
    }

    public RelationAssert<P, C> doesNotHaveChildParentReleation() {
        isNotNull();
        Assertions.assertThat(this.actual.child.getParent())
                .as("Expected child to have not relation to this parent")
                .isNotEqualTo(this.actual.parent);
        return this;
    }

    public RelationAssert<P, C> hasBidirectionalReleation() {
        isNotNull();
        return hasParentChildReleation().hasChildParentReleation();
    }

    /**
     * Assert that there is no releation between parent and child at all. 
     */
    public RelationAssert<P, C> doesNotHaveAReleation() {
        isNotNull();
        return doesNotHaveParentChildReleation().doesNotHaveChildParentReleation();
    }
}
