package de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.RelationAssert;
import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.TestScenario;
import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.TestScenarioBuilder;

/**
 * Test the Setup with: JoinColumn annotated instead of MappedBy attribute, from package
 * {@link de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn}.
 */
@DataJpaTest
public class JoinColumnTest {

    @PersistenceContext
    private EntityManager entityManager;

    private TestScenarioBuilder<ParentJc, ChildJc> testScenarioBuilder() {
        return new TestScenarioBuilder<>(this.entityManager, ParentJc::new, ChildJc::new);
    }

    /**
     * Just test that the mapping works in general:
     * create a full defined scenario ({@code parent.children = [child]} and {@code child.parent = parent})
     * save it, and reload it.
     * The reloaded entities are expected to have {@code children} and {@code parent} fields filled.
     */
    @Test
    public void testMapping() {
        /* given: a parent and its child, with both sided defined relationship that is saved */
        TestScenario<ParentJc, ChildJc> testScenario = testScenarioBuilder().build(true, true);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .hasBidirectionalReleation();

        /* when reloading parent and child */
        TestScenario<ParentJc, ChildJc> reload = testScenario.relead(this.entityManager);

        /* then: there is still a full bidirectional relationship between parent and child */
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).hasBidirectionalReleation();
    }

    /**
     * Scenario: store a inconsistent scenario where only the parent side ({@code Parent.children}) of the relation
     * is set in the stored entities.
     */
    @Test
    public void testInconsistencyOnlyParentDefined() {
        /* given: a parent and its child, with conflicting defined relationship: parent has a child, but the child has no parent. */
        TestScenario<ParentJc, ChildJc> testScenario = testScenarioBuilder().build(true, false);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .hasParentChildReleation()
                .doesNotHaveChildParentReleation();

        /* when reloading parent and child */
        TestScenario<ParentJc, ChildJc> reload = testScenario.relead(this.entityManager);

        /* then: relationship is "restored" on both sides */
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).hasBidirectionalReleation();
    }

    /**
     * Scenario: store a inconsistent scenario where only the child side ({@code Child.parent}) of the relation
     * is set in the stored entities.
     */
    @Test
    public void testInconsistencyOnlyChildDefined() {

        /* given a parent and its child, with conflicting defined relationship: parent has a child, but the child has no parent. */
        TestScenario<ParentJc, ChildJc> testScenario = testScenarioBuilder().build(false, true);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .doesNotHaveParentChildReleation()
                .hasChildParentReleation();

        /* when reloading parent and child */
        TestScenario<ParentJc, ChildJc> reload = testScenario.relead(this.entityManager);        
        
        /* then: relationship is not "restored" (because it never exists for Hibernate )*/
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).doesNotHaveAReleation();
    }
}
