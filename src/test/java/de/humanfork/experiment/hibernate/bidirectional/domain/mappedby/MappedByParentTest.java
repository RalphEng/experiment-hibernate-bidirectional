package de.humanfork.experiment.hibernate.bidirectional.domain.mappedby;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.RelationAssert;
import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.TestScenario;
import de.humanfork.experiment.hibernate.bidirectional.domain.testsupport.TestScenarioBuilder;

/**
 * Test the Setup with: Mapped-By annotated children field at parent entity, from package
 * {@link de.humanfork.experiment.hibernate.bidirectional.domain.mappedby}.
 */
@DataJpaTest
public class MappedByParentTest {

    @PersistenceContext
    private EntityManager entityManager;

    private TestScenarioBuilder<ParentMbP, ChildMbP> testScenarioBuilder() {
        return new TestScenarioBuilder<>(this.entityManager, ParentMbP::new, ChildMbP::new);
    }

    /**
     * Just test that the mapping works in general:
     * create a full defined scenario ({@code parent.children = [child]} and {@code child.parent = parent})
     * save it, and reload it.
     * The reloaded entities are expected to have {@code children} and {@code parent} fields filled.
     */
    @Test
    public void testMapping() {
        /* given: a parent and its child, with both sided defined relationship that is saved */
        TestScenario<ParentMbP, ChildMbP> testScenario = testScenarioBuilder().build(true, true);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .hasBidirectionalReleation();

        /* when reloading parent and child */
        TestScenario<ParentMbP, ChildMbP> reload = testScenario.relead(this.entityManager);

        /* then: there is still a full bidirectional relationship between parent and child */
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).hasBidirectionalReleation();
    }

    /**
     * Scenario: store a inconsistent scenario where only the parent side ({@code Parent.children}) of the relation
     * is set in the stored entities.
     */
    @Test
    public void testInconsistencyOnlyParentDefined() {
        /* given: a parent and its child, with conflicting defined relationship: parent has a child, but the child has no parent. */
        TestScenario<ParentMbP, ChildMbP> testScenario = testScenarioBuilder().build(true, false);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .hasParentChildReleation()
                .doesNotHaveChildParentReleation();

        /* when reloading parent and child */
        TestScenario<ParentMbP, ChildMbP> reload = testScenario.relead(this.entityManager);

        /* then: relationship is not "restored" (because it never exists for Hibernate )*/
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).doesNotHaveAReleation();
    }

    /**
     * Scenario: store a inconsistent scenario where only the child side ({@code Child.parent}) of the relation
     * is set in the stored entities.
     */
    @Test
    public void testInconsistencyOnlyChildDefined() {

        /* given a parent and its child, with conflicting defined relationship: parent has a child, but the child has no parent. */
        TestScenario<ParentMbP, ChildMbP> testScenario = testScenarioBuilder().build(false, true);
        RelationAssert.assertThat(testScenario.getParent(), testScenario.getChild())
                .doesNotHaveParentChildReleation()
                .hasChildParentReleation();

        /* when reloading parent and child */
        TestScenario<ParentMbP, ChildMbP> reload = testScenario.relead(this.entityManager);

        /* then: relationship is "restored" on both sides */
        RelationAssert.assertThat(reload.getParent(), reload.getChild()).hasBidirectionalReleation();
    }
}
