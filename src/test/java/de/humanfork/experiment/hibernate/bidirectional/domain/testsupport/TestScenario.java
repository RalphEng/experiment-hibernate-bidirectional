package de.humanfork.experiment.hibernate.bidirectional.domain.testsupport;

import javax.persistence.EntityManager;

import de.humanfork.experiment.hibernate.bidirectional.domain.Child;
import de.humanfork.experiment.hibernate.bidirectional.domain.Parent;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@NonNull
public class TestScenario<P extends Parent<C>, C extends Child<P>> {
    
    private final P parent;

    private final C child;
    
    
    public TestScenario<P, C> relead(@NonNull EntityManager entityManager) {
        entityManager.flush();
        entityManager.clear();

        @SuppressWarnings("unchecked")
        P reloadedParent = entityManager.find((Class<P>) parent.getClass(), parent.getId());
        @SuppressWarnings("unchecked")
        C reloadedChild = entityManager.find((Class<C>) child.getClass(), child.getId());

        /* assert a successful reload */
        PersonEntityAssert.assertThat(reloadedParent).isNotNull().hasEqualNameAndId(parent);
        PersonEntityAssert.assertThat(reloadedChild).isNotNull().hasEqualNameAndId(child);

        return new TestScenario<P, C>(reloadedParent, reloadedChild);
    }

}
