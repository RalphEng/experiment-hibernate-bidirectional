Experiment: Hibernate bidirectional mapping
===========================================

Test different ways of bidirectional mapping with Hibernate, and what happen if one modify only one side of the relation ship.

typical (right) way
-------------------

A typical (right) bidirectional mapping is done this way:
(See package: `de.humanfork.experiment.hibernate.bidirectional.domain.mappedby`)

```
@Entity
public class Parent {

   @OneToMany(mappedBy = "parent")
   private List<Child> children;
}
```

```
@Entity
public class Child {

   @ManyToOne
   private Parent parent;
}
```

The many side (`Client.parent`) is the one that is the maintaining / leading side.
This mean, that Hibernate is only interested in this information when it comes to save the relationship.
So if there is a inconsistency between `Parent.children` and `Client.parent` then `Client.parent` win.
 

the other way
-------------

If one want to make the `Parent.children` collection the leading side, then one has to abuse Hibernate a bit:
One has to configure the `Parent.children` like it is a unidirectional releationship maintained by `Parent.children`.
And then one must map `Client.parent` to the same FK column, but with readonly mode.

(See package: `de.humanfork.experiment.hibernate.bidirectional.domain.joincolumn`)

```
@Entity
public class Parent {

   @OneToMany
   @JoinColumn(name = "parentFK")
   private List<Child> children;
}
```

```
@Entity
public class Child {

   @ManyToOne
   @JoinColumn(name = "parentFK", insertable = false, updatable = false) 
   private Parent parent;
}
```



Lombok
------

This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

### Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
>An unexpected exception occurred while performing the refactoring.
>
>Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536
